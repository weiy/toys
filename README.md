# This is a project on how to generate and scan Neyman constructions i.e. toys.

## The workflow is to (1) generate profiled workspaces (2) generate toy workspace, (3) scan and (4) plot.

## Step 1: generate profiled workspaces at each mu for scan

1. In __generate_profiled_ws__ dir:

    - generate input cards for different mu's: `cd Card && python generate_toy_xml.py &&  cd ..`

    - generate profiled workspaces: `bash generate_profiled_ws.sh`. In practice, we need separate this script as about 5 scripts to reduce the runtime on lxplus.

## Step 2: run local test and generate grid jobs

1. In __jobs__ dir:
        
    - `setup.sh`: to source HZZ code,

    - THIS SCIRPT HAS BEEN REPLACE BY quickToy but is useful for understanding: `throwToyswNP.cxx`: script to generate MC toy workspace,

    - `generate_toys.py`: generate MC toy workspace by calling `throwToyswNP.cxx`, for __local__ test.

    - `generate_toys_winput.py` and `generate_jobList.py`: generate grid submission job lists.

1. How to use

    - local test: run `generate_toys.py`

    - grid: 

        - run `generate_jobList.py`: remember to modify poi range and intervals as you would like: 
            `mus = [round(mu*0.1, 2) for mu in range(0, 50+1)]` 

        - remember to change which poi you would like to generate in `generate_toys_winput.py`.

        - this step is only for script job generation.

## Step 3: submit massive grid jobs
    
1. `cd grid_submit`
    
1. `bash create_toys_inputs.sh` #copy grid jobs generated before

1. take a look at command.sh for grid submission.


## Step 4: download and plot NLL scans.

1. Grid download the `WS*.root` files into a directory with large space and untar.

1. Go to `drawplot` dir

1. Read README.md
