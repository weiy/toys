import os
import argparse

parser = argparse.ArgumentParser(description='tar')
parser.add_argument('--input_files',    type=str, default="", help='')

args = parser.parse_args()

print("args.input_files")
print(args.input_files)

for f in args.input_files.split(","):
    if 'tgz' in f:
        os.system("tar -zxvf "+ f)
