export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

#it's not useful for following "if"
#because each sub job will rebuild in a new dir

home_dir=`pwd`

if [ ! -d toys ]; then
    tar -zxvf toys.tgz.gz
fi
if [ ! -d  HZZWorkspace ]; then
    tar -zxvf HZZWorkspace.tgz.gz
fi
if [ ! -d  quickFit ]; then
    tar -zxvf quickFit.tgz.gz
fi

# cd quickFit
# bash install.sh
# cd ${home_dir}
# echo "build quickFit done!"
# echo "sleep 10s"
# sleep 60s

# cd HZZWorkspace/build
# w
# if [ ! -f x86_64-centos7-gcc8-opt/setup.sh ]; then
#     cmake ../source
#     make
# fi
# source x86_64-centos7-gcc8-opt/setup.sh
# cd ../../

cd HZZWorkspace/build
asetup AnalysisBase,21.2.189,here
source x86_64-centos7-gcc8-opt/setup.sh
cd ../../
echo "build HZZWorkspace done!"
scan_poi --help

pwd
echo "cd quickFit"
cd quickFit
pwd
ls -lh
ulimit -S -s unlimited
export _DIRFIT=${PWD}
export _BIN_PATH=${_DIRFIT}/bin
export _LIB_PATH=${_DIRFIT}/lib
export LD_LIBRARY_PATH=${_LIB_PATH}:${LD_LIBRARY_PATH}
export PATH=${_BIN_PATH}:${PATH}
cd ..

#The following is wrong!!! I don't know why
#Grid job can not find setup_lxplus_source.sh
# # source setup_lxplus_source.sh
# source setup_lxplus.sh
# cd ${home_dir}
quickToy --help
