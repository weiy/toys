export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

bash create_toys_inputs.sh

#rucio add-dataset user.weiy.uploaded1
#rucio upload --rse CERN-PROD_SCRATCHDISK --scope user.weiy WS_Gamma_201022_mu0p0_toy10.root
#rucio attach user.weiy.uploaded1 user.weiy:WS_Gamma_201022_mu0p0_toy51.root

voms
# prun --exec "source setup.sh | tee setup.log" --outDS user.${USER}.toys_test5 --outputs *setup.log* --extFile HZZWorkspace.tgz.gz,setup.sh

#user submit
prun --exec "pwd; ls -lh; tar -zxvf HZZWorkspace.tgz.gz; tar -zxvf quickFit.tgz.gz; cd quickFit; bash install.sh; cd -; cd HZZWorkspace; bash install.sh; cd -; source setup.sh ; bash grid_run.sh %RNDM:0" \
    --outDS user.${USER}.toys_Gamma_10ktoys_wscan5_test \
    --outputs Scan*.root --nJobs 5800 \
    --extFile HZZWorkspace.tgz.gz,setup.sh,toys.tgz.gz \

    # --mergeOutput
    # never mergeOutput because this is quite slow. MergeOupt by ourselves instead. See the last command block.

#group submit
prun --exec "pwd; ls -lh; tar -zxvf HZZWorkspace.tgz.gz; tar -zxvf quickFit.tgz.gz; cd quickFit; bash install.sh; cd -; cd HZZWorkspace; bash install.sh; cd -; source setup.sh ; bash grid_run.sh %RNDM:0" \
    --official \
    --voms=atlas:/atlas/phys-higgs/Role=production \
    --outDS group.phys-higgs.user.${USER}.toys_Gamma_10ktoys_wscan_fixedmuVBFmuggF \
    --outputs Scan*.root --nJobs 127500 \
    --extFile HZZWorkspace.tgz.gz,setup.sh,toys.tgz.gz \

    # --mergeOutput
    # never mergeOutput because this is quite slow. MergeOupt by ourselves instead. See the last command block.


#NOTE that even merge output by ourselves in below, it still takes several hours and have a lot of problem like less efficiency. Therefore we'd better just download handreds of thousands files directly AT LXPLUS because this is faster and then ssh rsync to pplxint. (It will take one or two hours).

#mergeOutput by ourselves
# prun --exec "python merge.py --input_files %IN" \
#     --official \
#     --voms=atlas:/atlas/phys-higgs/Role=production \
#     --inDS user.${USER}.toys_Rgg_profiled_1ktoys_wscan_full3_ScanXYZ.root.tgz \
#     --outDS group.phys-higgs.user.${USER}.toys_Rgg_profiled_1ktoys_wscan_merged_test5 \
#     --allowTaskDuplication \
#     --nJobs 50 \
#     --forceStaged \
#     --noBuild \
#     --disableAutoRetry \
#     --outputs Scan*.root \
#     --notExpandInDS \
#     --extFile merge.py

# --forceStaged is very important which will helps us bypass root check because our inDS is root.tgz file
# --notExpandInDS is also important I think?
# --noBuild to avoid exhausted case as all the jobs are less than 30 minutes.


