import os
import numpy as np

mu_list=np.arange(0, 5.1, 0.1).round(2).tolist()
print(mu_list)
mu_list_str=[str(i).replace('.','p') for i in mu_list]
print(mu_list_str)

for i in range(len(mu_list)):
    print('generating '+str(mu_list[i]))
    f_tmp=open('Card_profiledToData_4l2l2v_mu_toy_tmp.xml','r')
    f_out=open('toys/Card_profiledToData_4l2l2v_mu_toy_mu'+mu_list_str[i]+'.xml','w')
    for line in f_tmp:
        f_out.write(line.replace('strTOYMU',mu_list_str[i]).replace('TOYMU',str(mu_list[i])))
