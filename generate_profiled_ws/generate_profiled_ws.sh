if [ -z $SOURCEQUICKFIT ];
then
    cd /afs/cern.ch/user/w/weiy/offshell/quickFit
    source setup_lxplus.sh
    cd -
    SOURCEQUICKFIT=1
fi

mus=(0p0 0p1 0p2 0p3 0p4 0p5 0p6 0p7 0p8 0p9 1p0 1p1 1p2 1p3 1p4 1p5 1p6 1p7 1p8 1p9 2p0 2p1 2p2 2p3 2p4 2p5 2p6 2p7 2p8 2p9 3p0 3p1 3p2 3p3 3p4 3p5 3p6 3p7 3p8 3p9 4p0 4p1 4p2 4p3 4p4 4p5 4p6 4p7 4p8 4p9 5p0)

for mu in ${mus[@]};do
    echo "generating mu "${mu}
    quickAsimov -x Card/toys/Card_Profiled_4l2l2v_mu_toy_mu${mu}.xml -w combWS -m ModelConfig -d combData --numCPU 10 --minStrat 2  --minTolerance 0.0001 --nllOffset true | tee profiled_ws/Card_Profiled_4l2l2v_mu_toy_mu${mu}.log
done
