#include "TFile.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "TString.h"
#include "TRandom2.h"
#include "TRandom3.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/ToyMCSampler.h"
#include "RooRandom.h"
#include "RooSimultaneous.h"
#include "RooProdPdf.h"
#include "TROOT.h"

using namespace RooFit; 
using namespace std;

double changeExpectedEvents(double Nbefore) {
    TRandom2* rdm = new TRandom2();
    rdm->SetSeed();
    // cout << "Poisson TOYS: events: " << Nbefore << endl;

    double Nafter = rdm->PoissonD(Nbefore);
    if (Nafter < 0) { Nafter = 0.; }
    // cout << "Poisson TOYS: after: " << Nafter << endl;

    return Nafter;
}

double changeNPvalue(TString mynpname) {
    TRandom2* rdm = new TRandom2();
    rdm->SetSeed();
    // unit Gaussian
    double Nafter = rdm->Gaus(0,1);
    // cout << "NP: "<< mynpname.Data() << " set to: " << Nafter << endl;

    return Nafter;
}

void throw_toys( RooWorkspace*& workspace, double poi_value, const char* np_list = "_top", int channel = 0) {
    cout << "About to change the expected number of events in each bin" << endl;

    // options for channel (2l2v as default)  
    bool docombined = false;
    bool do4l = false;
    bool doWidth = false;
    bool doggF = false;
    bool doVBF = false;

    if (channel == 3) {
        doWidth = true;
        docombined = true; // for tests
    }

    if (channel == 2) {
        docombined = true;
    }
    else if (channel == 1) {
        do4l = true;
    }

    if (channel == 10) {
        doggF = true;
        docombined = true;
    }

    if (channel == 20) {
        doVBF = true;
        docombined = true;
    }

    TString muName = "mu";
    if (doWidth){ muName = "Gamma"; }
    if (doggF){ muName = "mu_ggF"; }
    if (doVBF){ muName = "mu_VBF"; }

    cout << "poi:"<< muName.Data() << endl;

    RooRealVar* poi = (RooRealVar*) workspace->var(muName.Data());
    // poi->Print();
    poi->setVal(poi_value);

    TString datasetName = "asimovData";
    if (docombined) { datasetName = "asimovDatamu"; }
    if (doWidth) { datasetName = "combData"; }

    RooDataSet* asimov = (RooDataSet*)workspace->data(datasetName.Data());

    RooDataSet* newDataset = (RooDataSet*) asimov->emptyClone();
    newDataset->SetName("newDataset");

    // exit(10);

    RooRealVar *m4l, *ggFNN, *VBFNN, *n_jets, *mT_ZZ, *m4_, *ggNN_, *VBFNN_, *j_, *m_;

    if (do4l) {
        m4l = workspace->var("m4l_fsr");
        ggFNN  = workspace->var("ggFNN_MELA");
        VBFNN  = workspace->var("VBFNN_MELA");
    } else if (docombined) {
        m4l = workspace->var("m4l_fsr");
        ggFNN  = workspace->var("ggFNN_MELA");
        VBFNN  = workspace->var("VBFNN_MELA");
        n_jets = workspace->var("n_jets");
        mT_ZZ  = workspace->var("mT_ZZ");
    } else {
        n_jets = workspace->var("n_jets");
        mT_ZZ  = workspace->var("mT_ZZ");
    }

    TString channelname = "channelCat";
    if (docombined) {
        channelname = "combCat";
    } 

    RooCategory *channelCat = workspace->cat(channelname.Data());

    if (do4l) {
        m4_ = (RooRealVar *) m4l->Clone("m4l_fsr");
        ggNN_ = (RooRealVar *) ggFNN->Clone("ggFNN_MELA");
        VBFNN_ = (RooRealVar *) VBFNN->Clone("VBFNN_MELA");
    } else if (docombined) {
        m4_ = (RooRealVar *) m4l->Clone("m4l_fsr");
        ggNN_ = (RooRealVar *) ggFNN->Clone("ggFNN_MELA");
        VBFNN_ = (RooRealVar *) VBFNN->Clone("VBFNN_MELA");
        j_ = (RooRealVar *) n_jets->Clone("n_jets");
        m_ = (RooRealVar *) mT_ZZ->Clone("mT_ZZ");
    } else {
        j_ = (RooRealVar *) n_jets->Clone("n_jets");
        m_ = (RooRealVar *) mT_ZZ->Clone("mT_ZZ");
    }

    // exit(10);

    RooCategory *c_ = (RooCategory *) channelCat->Clone(channelname.Data());
    // exit(10);

    RooStats::ModelConfig*  mc_config;
    RooSimultaneous* simPdf;
    RooAbsPdf * mypdf;
    string label;
    double nEvt, sf, newNPvalue;

    // validation for first ggF bin: print some before values for the pdf and the NP
    TString ggFbinName = "SR_ggF_Incl_bin_2_";
    int ggFbin = 13;
    // cout << "ggF first bin Before" << endl;
    mc_config = (RooStats::ModelConfig*) workspace->obj("ModelConfig");
    // mc_config->Print();
    // simPdf = dynamic_cast<RooSimultaneous*>(mc_config->GetPdf());
    // simPdf->Print();
    // label = string(asimov->get(ggFbin)->getCatLabel(channelname.Data()));
    // mypdf = simPdf->getPdf(label.data()); //RooProdPdf
    // mypdf->Print();
    // asimov->get(ggFbin);
    // cout << "Asimov events before:" << asimov->weight() <<endl;

    // cout << "End of before\n" << endl;


    RooRealVar* thisNP;
    vector<TString> nps;

    cout << "about to fill all NPs" << endl;

    if (TString(np_list).Contains("statOnly")) {
        nps = {};
    }

    // if requested, make a list with all NPs in the WS
    // else if (TString(np_list).Contains("allNP")) {
    else {
        nps = {};
        RooArgSet* allNuis = (RooArgSet*)mc_config->GetNuisanceParameters();
        TIterator* itr(allNuis->createIterator());
        while ((thisNP = (RooRealVar*)itr->Next())) {
            TString mynpname = thisNP->GetName();
            if (!mynpname.Contains("LUMI") && !mynpname.Contains("MCSTAT") && !mynpname.Contains("MCStat")) { 
                nps.push_back(mynpname);
            }
        }
    }
    cout << "filled all NPs" << endl;

    map<string, double> mapNPs;

    // before changing Nevents using Poisson, fluctuate the NPs and save them in a map
    for (TString mynpname : nps) {

        // change the relevant NP
        RooRealVar* mynp = workspace->var(mynpname.Data());

        // check if the NP exists in the WP
        if (!mynp) {
            cout << "Could not find NP: " << mynpname.Data() << " in the Workspace" << endl;
            cout << np_list << endl;
            exit(10);
        }

        // cout << "about to change this NP:" << endl;
        // mynp->Print();

        // take a random number from the unit Gaussian
        newNPvalue = changeNPvalue(mynpname);
        // or, for tests, set it to any value
        // newNPvalue = 0.;

        mapNPs[mynp->GetName()] = newNPvalue;

        // mynp->setVal(newNPvalue);
        // cout << "Changed to:" << endl;
        // mynp->Print();

    }
    cout << "Nuisance parameter values in this toy:" <<endl;

    // check the contents of the map
    for (auto np_and_value : mapNPs) {
        cout << "NP: " << np_and_value.first << ", value: " << np_and_value.second << endl;
    }
    cout << "\n";

    cout << asimov->numEntries()<< endl;

    // build a map of yield per bin from the asimov dataset, where we will store the yields for the new dataset
    map<TString, double> yieldPerBin;
    for (int i = 0; i < asimov->numEntries(); i++) {
        asimov->get(i);
        TString label = TString(asimov->get(i)->getCatLabel(channelname.Data()));
        cout << label.Data() << endl;

        TString bin = label.ReplaceAll("Cat", "");
        if (docombined) {
            bin = bin.ReplaceAll("comb_channel_4l_", "");
            bin = bin.ReplaceAll("comb_channel_2l2v_", "");
        }
        cout << bin.Data() << endl;

        // start at 0, and add yield per process later
        yieldPerBin[bin] = 0.;
    }

    cout << "Normalization per process, per bin:" <<endl;
    // now vary the yield per bin, per process, and add to the map
    RooArgSet* allNorms = (RooArgSet*)workspace->allFunctions().selectByName("nTot*");
    TIterator* iter(allNorms->createIterator());
    double before_norm, after_norm = 0.;
    TString curr_process;
    for (RooAbsArg* a = (RooAbsArg*)iter->Next(); a!=0; a=(RooAbsArg*)iter->Next()){
        RooAbsReal* process_per_bin = dynamic_cast<RooAbsReal*>(a);

         // before
        curr_process = process_per_bin->GetName();
        before_norm = process_per_bin->getVal();

        // for tests
        // if (!curr_process.Contains(ggFbinName)) { continue; }
        cout << "Normalization of " << curr_process << " at nominal: " << before_norm << endl;

        // set NP
        RooArgSet* allNuis = (RooArgSet*)mc_config->GetNuisanceParameters();
        TIterator* iterNP(allNuis->createIterator());
        while ((thisNP = (RooRealVar*)iterNP->Next())) {
            // if requested, change NP value
            if (mapNPs.count(thisNP->GetName())) {
                thisNP->setVal(mapNPs[thisNP->GetName()]);
            }

            // check what the change is per process per bin per NP
            // if (curr_process.Contains(bin) ) {
            //     cout << "Normalization of " << curr_process << " after varying NP " << thisNP->GetName() << ": "  << after_norm << endl;
            // }
        }



        // get the new value for the norm
        after_norm = process_per_bin->getVal();

        // add it to the yield map
        for ( auto bin_and_yield : yieldPerBin) {
            TString bin = bin_and_yield.first;
            // cout << "bin: " << bin << ", process " << curr_process <<  endl;
            if (curr_process.Contains(bin) ) {
                cout << "Normalization of " << curr_process << " after varying NPs: " << after_norm << endl;
                yieldPerBin[bin] = yieldPerBin[bin] + after_norm;
            }
        }

        iterNP->Reset();
        while ((thisNP = (RooRealVar*)iterNP->Next())) {
            // cout << thisNP->GetName() << thisNP->getVal() << endl;
            // note: this one is not actually a gamma param: ATLAS_gamma_MCStat_0jet_Pt4l_GE100_4l_H4l_redbkg
            if (TString(thisNP->GetName()).Contains("MCSTAT") ) {
                thisNP->setVal(1); 
            } else {
                thisNP->setVal(0);
            }
            thisNP->setConstant(false);
        }
    }
    cout << "\n";

    cout << "About to build the new dataset via poisson fluctuations:" <<endl;
    // build new dataset from yieldPerBin map
    for (int i = 0; i < asimov->numEntries(); i++) {
        asimov->get(i);

        // for now, only look at the first ggF bin
        // if (i != ggFbin) {continue;}

        TString label = asimov->get(i)->getCatLabel(channelname.Data());
        TString bin = label.ReplaceAll("Cat", "");
        if (docombined) {
            bin = bin.ReplaceAll("comb_channel_4l_", "");
            bin = bin.ReplaceAll("comb_channel_2l2v_", "");
        }


        nEvt = yieldPerBin[bin];
        cout << "Now in bin: " << label <<endl;
        cout << "Asimov events:" << asimov->weight() <<endl;
        cout << "Events after varying NPs: " << nEvt <<endl;


        // now do the Poisson fluctuation
        // turn off to test the NPs
        nEvt = changeExpectedEvents(nEvt);
        cout << "Number of events after Poisson:" << nEvt <<endl;


        // Build the new dataset, with the new event yield
        if (do4l) {
            m4_->setVal(asimov->get(i)->getRealValue("m4l_fsr"));
            ggNN_->setVal(asimov->get(i)->getRealValue("ggFNN_MELA"));
            VBFNN_->setVal(asimov->get(i)->getRealValue("VBFNN_MELA"));
        } else if (docombined) {
            m4_->setVal(asimov->get(i)->getRealValue("m4l_fsr"));
            ggNN_->setVal(asimov->get(i)->getRealValue("ggFNN_MELA"));
            VBFNN_->setVal(asimov->get(i)->getRealValue("VBFNN_MELA"));
            j_->setVal(asimov->get(i)->getRealValue("n_jets"));
            m_->setVal(asimov->get(i)->getRealValue("mT_ZZ"));            
        } else {
            j_->setVal(asimov->get(i)->getRealValue("n_jets"));
            m_->setVal(asimov->get(i)->getRealValue("mT_ZZ"));            
        }
        int idx = asimov->get(i)->getCatIndex(channelname.Data());
        c_->setIndex(idx);

        if (docombined) {
            newDataset->add( RooArgList(*m4_,*ggNN_,*VBFNN_,*j_,*m_,*c_), nEvt );
        } else if (do4l) {
            newDataset->add( RooArgList(*m4_,*ggNN_,*VBFNN_,*c_), nEvt );
        } else {
            newDataset->add( RooArgList(*j_,*m_,*c_), nEvt );
        }
    }

    // reset the poi
    poi->setVal(1.);

    // for tests, to prevent writing to the WS
    // return;

    // save the WS with the new dataset
    workspace->import(*newDataset);
    // workspace->Write("combined",TObject::kOverwrite); 
    // workspace->Write(WSname.Data(),TObject::kOverwrite); 
    // workspace->Write("newDataset",TObject::kOverwrite); 
    workspace->Write("combWS",TObject::kOverwrite); 


}

// for the width, need a more general approach, since we also want to vary the onshell parameters, and the onshell WS has a different architecture
void throw_toys_width( RooWorkspace*& workspace, double poi_value, int channel = 3, TString WSname = "combWS") {
    cout << "About to change the expected number of events in each bin" << endl;

    // options for channel (2l2v/4l as default)  
    bool docombined = false;
    bool doWidth = false;
    bool doRvv = false;
    bool doRgg = false;
    bool doggF = false;
    bool doVBF = false;

    if (channel == 3) {
        doWidth = true;
        docombined = true;
    }

    if (channel == 2) {
        docombined = true;
    }

    if (channel == 4) {
        doRvv = true;
        docombined = true;
    }

    if (channel == 5) {
        doRgg = true;
        docombined = true;
    }

    if (channel == 10) {
        doggF = true;
        docombined = true;
    }

    if (channel == 20) {
        doVBF = true;
        docombined = true;
    }

    TString muName = "mu";
    if (doWidth){ muName = "Gamma"; }
    if (doRgg){ muName = "Rgg"; }
    if (doRvv){ muName = "Rvv"; }
    if (doggF){ muName = "mu_ggF"; }
    if (doVBF){ muName = "mu_VBF"; }

    cout << "poi:"<< muName.Data() << endl;

    RooRealVar* poi = (RooRealVar*) workspace->var(muName.Data());
    RooStats::ModelConfig* mc_config = (RooStats::ModelConfig*) workspace->obj("ModelConfig");

    // poi->Print();
    poi->setVal(poi_value);

    cout << "set NPs" << endl;

    double newNPvalue;
    TString mynpname;
    RooRealVar* thisNP;
    RooArgSet* allNuis = (RooArgSet*)mc_config->GetNuisanceParameters();
    TIterator* itr(allNuis->createIterator());
    while ((thisNP = (RooRealVar*)itr->Next())) {
        mynpname = thisNP->GetName();
        if (!mynpname.Contains("LUMI") && !mynpname.Contains("MCSTAT") && !mynpname.Contains("MCStat")) { 
            newNPvalue = changeNPvalue(mynpname);
            thisNP->setVal(newNPvalue);
            cout << mynpname.Data() << " set to: " << newNPvalue << endl;
        }
    }

    // cout << "making toys with" << endl;
    // mc_config->GetObservables()->Print();
    // mc_config->GetNuisanceParameters()->Print();

    TString channelname = "channelCat";
    if (docombined) {
        channelname = "combCat";
    }


    RooArgSet* obsList= (RooArgSet*) mc_config->GetObservables();
    if (!obsList->find(channelname.Data())){
        obsList->add(*(dynamic_cast<RooAbsArg*>(workspace->obj(channelname.Data())))); 
    } 

    RooStats::ToyMCSampler* toy = new RooStats::ToyMCSampler();
    toy->SetPdf( *mc_config->GetPdf() );
    toy->SetObservables( *obsList );
    toy->SetNuisanceParameters(*mc_config->GetNuisanceParameters());

    // TRandom3* randomGenerator = new TRandom3();
    RooRandom::randomGenerator()->SetSeed();
    // randomGenerator->SetSeed();

    RooDataSet* newDataset = (RooDataSet*) toy->GenerateToyData(*obsList);
    // auto newDataset = toy->GenerateToyData(*obsList);    
    newDataset->SetName("newDataset");

    // ideas
    // print yields first few pdfs? or some first events with obs and values
    // check if gamma should be 0 or 1?
    // plot pdfs from modelconfig?
    // use original WS for fit 
    // diff between printouts WS

    // reset the poi and NPs
    cout << poi->GetName() <<": "<<poi->getVal() << endl;
    poi->setVal(1.);
    itr->Reset();
    while ((thisNP = (RooRealVar*)itr->Next())) {
        // cout << thisNP->GetName() << thisNP->getVal() << endl;
        // note: this one is not actually a gamma param: ATLAS_gamma_MCStat_0jet_Pt4l_GE100_4l_H4l_redbkg
        if (TString(thisNP->GetName()).Contains("MCSTAT") ) {
            thisNP->setVal(1); 
        } else {
            thisNP->setVal(0);
        }
        thisNP->setConstant(false);
    }


    // for tests, to prevent writing to the WS
    // return;

    // save the WS with the new dataset
    workspace->import(*newDataset);
    workspace->Write(WSname.Data(),TObject::kOverwrite); 
    // workspace->writeToFile("tmp_WS.root"); 

}


int throwToyswNP(const char* filename, double mu = 4.5, const char* np_list = "_top", int channel = 0)
{
    gROOT->SetBatch();
    // TFile *file = new TFile("Obs_test/Output/WS_test_statOnlyObs.root", "UPDATE");
    TFile *file = new TFile(filename, "UPDATE");
    TString WSname = "combined";
    if (channel >= 2) { WSname = "combWS"; }
    cout << WSname << endl;
    RooWorkspace *ws = (RooWorkspace*) file->Get(WSname);
    if (channel > 2) {
        throw_toys_width(ws, mu, channel, WSname);
    } else {
        throw_toys(ws, mu, np_list, channel);
    }
    delete file;
    return 0;
}
