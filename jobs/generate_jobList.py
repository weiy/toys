import os
import multiprocessing as mp

#poi range and intervals
mus = [round(mu*0.1, 2) for mu in range(0, 50+1)]
# mus = [2.4]

#number of toys for each poi
n_toys_permu=1000

#number of toy scans in each grid job
n_toys_sub=4

os.system("rm -rf jobList")
os.system("mkdir jobList")

def generate_mus(n):
    #n_jobs start value
    n_jobs=n*len(mus)/n_toys_sub #+100
    for mu in mus:
        # print("Generating mu="+str(mu)+", toy="+str(n))
        f_jobs=open("jobList/gridJob_"+str(n_jobs)+".sh","w")
        cmd="python generate_toys_winput.py --mu "+str(mu)+" --nstart "+str(n)+" --nend "+str(n+n_toys_sub)
        print(cmd)
        f_jobs.write("#!/bin/bash\n")
        f_jobs.write(cmd)
        f_jobs.close()
        os.system("chmod +x jobList/gridJob_"+str(n_jobs)+".sh")
        n_jobs+=1

ith_toys=range(0,n_toys_permu,n_toys_sub)
pool = mp.Pool(processes=30)
results = pool.map(generate_mus, ith_toys)
pool.close()

print("\nTotal number of jobs is "+str(len(mus)*n_toys_permu/n_toys_sub))
