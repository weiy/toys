import os
import argparse

parser = argparse.ArgumentParser(description='run joblist')
parser.add_argument('--job_i',    type=int, default=0, help='i-th of job')

args = parser.parse_args()

os.system("bash jobList/gridJob_"+str(args.job_i)+".sh")
