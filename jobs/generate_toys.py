import os

# cwd=os.getcwd()
script_path=os.path.dirname(os.path.realpath(__file__))
#need to define which mu
#in one whole job:
#mu=0-5
config={}

config["mu"]={"ws_path":"combined/WS_4l_2l2v_mu_201022_toy.root",
              "channel":"2",
              "wsName":"combWS",
              "muName":"mu",
              "dataName":"newDataset",
              "const":"mu_ggF:1.0,mu_VBF:1.0"}

config["Gamma"]={"ws_path":"combined/WS_Gamma_201022_toy.root",
                 "channel":"3",
                 "wsName":"combWS",
                 "muName":"Gamma",
                 "dataName":"newDataset",
                 "const":"mu_ggF:1.0,mu_VBF:1.0"}

config["Rvv"]={"ws_path":"combined/WS_RggRvv_281022_toy.root",
               "channel":"4",
               "wsName":"combWS",
               "muName":"Rvv",
               "dataName":"newDataset",
               "const":"Rgg:1.0"}

config["Rvv_stat"]={"ws_path":"combined/WS_RggRvv_281022_toy_stat.root",
                    "channel":"4",
                    "wsName":"combWS",
                    "muName":"Rvv",
                    "dataName":"newDataset",
                    "const":"Rgg:1.0"}

config["Rgg"]={"ws_path":"combined/WS_RggRvv_281022_toy.root",
               "channel":"5",
               "wsName":"combWS",
               "muName":"Rgg",
               "dataName":"newDataset",
               "const":"Rvv:1.0"}


poi="Gamma"
poi="mu"
poi="Rvv"
poi="Rvv_stat"

n_toys=100
n_toys=1
mus = [mu * 0.1 for mu in range(0, 50+1)]
mus=[0]
for mu in mus:
    for i in range(n_toys):
        print(poi+"="+str(mu)+", toy="+str(i))
        # rootpath_orig=script_path+'/originalWS/combined/WS_Gamma_201022_toy.root'
        # rootpath_new=script_path+'/Generated_toys/combined/WS_Gamma_201022_mu'+str(mu).replace('.','p')+'_toy'+str(i)+'.root'
        rootpath_orig=script_path+'/originalWS/'+config[poi]["ws_path"]
        rootpath_new=script_path+'/Generated_toys/'+config[poi]["ws_path"].replace('.root','').replace('_toy','')+'_mu'+str(mu).replace('.','p')+'_toy'+str(i)+'.root'
        os.system('cp '+rootpath_orig+' '+rootpath_new)

        os.system("root -n -l -q -b '"+script_path+"/throwToyswNP.cxx+(\"" +rootpath_new + "\", "+str(mu)+", \"_top5\", "+config[poi]["channel"]+")'")
        inname=rootpath_new
        out_name=rootpath_new.replace("WS_","Scan_WS_")

        wsName=config[poi]["wsName"]
        muName=config[poi]["muName"]
        dataName=config[poi]["dataName"]

        scan_cmd="scan_poi "+inname+" "+out_name+" "+wsName+" "+muName+" "+dataName+" "+muName+":50:0:5 "+config[poi]["const"]
        os.system(scan_cmd)

print("Scan finished!")
