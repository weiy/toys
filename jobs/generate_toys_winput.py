import os
import argparse
import random

script_path=os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description='generate joblist')
parser.add_argument('--mu',    type=float, default=0, help='input value mu')
parser.add_argument('--nstart',    type=int, default=0, help='start of toys')
parser.add_argument('--nend',    type=int, default=0, help='end of toys')

args = parser.parse_args()

mu=args.mu
nstart=args.nstart
nend=args.nend


config={}


config["Rvv"]={#"ws_fname":"WS_RggRvv_281022_toy.root",
               "ws_fname":"RggRVVlists/WS_RggRvv_161022final_toy_profiledDataNPs_RvvTOYMU.root",
               "channel":"4",
               "wsName":"combWS",
               "muName":"Rvv",
               "dataName":"newDataset",
               "const":"Rgg:1.0"}

config["Rvv_stat"]={"ws_fname":"WS_RggRvv_281022_toy_stat.root",
                    "channel":"4",
                    "wsName":"combWS",
                    "muName":"Rvv",
                    "dataName":"newDataset",
                    "const":"Rgg:1.0"}

config["Rvv_stat_fixed"]={"ws_fname":"WS_RggRvv_161022final_toy_stat.root",
                          "channel":"4",
                          "wsName":"combWS",
                          "muName":"Rvv",
                          "dataName":"newDataset",
                          "const":"Rgg:1.0,mu_VBF_On:1.0"}

config["Rgg"]={#"ws_fname":"WS_RggRvv_281022_toy.root",
               "ws_fname":"RggRVVlists/WS_RggRvv_161022final_toy_profiledDataNPs_RggTOYMU.root",
               "channel":"5",
               "wsName":"combWS",
               "muName":"Rgg",
               "dataName":"newDataset",
               "const":"Rvv:1.0"}

config["Gamma"]={#"ws_fname":"WS_Gamma_201022_toy.root",
                 "ws_fname":"Gammalists/WS_Gamma_161022final_toy_profiledDataNPs_GammaTOYMU.root",
                 "channel":"3",
                 "wsName":"combWS",
                 "muName":"Gamma",
                 "dataName":"newDataset",
                 "const":"mu_ggF:1.0,mu_VBF:1.0"}

config["Gamma_stat"]={"ws_fname":"WS_Gamma_201022_toy_stat.root",
                      "channel":"3",
                      "wsName":"combWS",
                      "muName":"Gamma",
                      "dataName":"newDataset",
                      "const":"mu_ggF:1.0,mu_VBF:1.0"}

config["mu"]={#"ws_fname":"WS_4l_2l2v_mu_201022_toy.root",
              "ws_fname":"mulists/WS_4l_2l2v_mu_161022_toy_profiledDataNPs_muTOYMU.root",
              "channel":"2",
              "wsName":"combWS",
              "muName":"mu",
              "dataName":"newDataset",
              "const":"mu_ggF:1.0,mu_VBF:1.0"}

config["mu_stat"]={"ws_fname":"WS_4l_2l2v_mu_201022_toy_stat.root",
                   "channel":"2",
                   "wsName":"combWS",
                   "muName":"mu",
                   "dataName":"newDataset",
                   "const":"mu_ggF:1.0,mu_VBF:1.0"}

# poi="Gamma"
# poi="Gamma_stat"
poi="mu"
# poi="mu_stat"
# poi="Rvv"
# poi="Rgg"
# poi="Rvv_stat"
# poi="Rvv_stat_fixed"

for i in range(nstart,nend):
    rand=random.randint(0,1e8)
    print("\nStart generating: mu="+str(mu)+", toy="+str(i)+"/"+str(nend-nstart))
    #manual code
    # rootpath_orig=script_path+'/originalWS/combined/'+config[poi]["ws_fname"]
    # rootpath_new=script_path+'/../'+config[poi]["ws_fname"].replace('.root','').replace('_toy','')+'_mu'+str(mu).replace('.','p')+'_toy'+str(i)+'.root'
    # os.system('cp '+rootpath_orig+' '+rootpath_new)
    # os.system("root -n -l -q -b '"+script_path+"/throwToyswNP.cxx+(\"" +rootpath_new + "\", "+str(mu)+", \"_top5\", "+config[poi]["channel"]+")'")

    rootpath_orig=script_path+'/originalWS/combined/'+config[poi]["ws_fname"].replace('TOYMU',str(mu).replace('.','p'))
    rootpath_new=script_path+'/../'+config[poi]["ws_fname"].replace('TOYMU',str(mu).replace('.','p')).split('/')[1].replace('.root','').replace('_toy','')+'_toy'+str(i)+'.root'

    os.system("quickToy -f "+rootpath_orig+" -d newDataset -p "+config[poi]["muName"]+"="+str(mu)+" -s conditionalGlobs_1,conditionalNuis_1 -o "+rootpath_new+" --seed "+str(rand))

    inname=rootpath_new
    out_name=rootpath_new.replace("WS_","Scan_WS_")

    wsName=config[poi]["wsName"]
    muName=config[poi]["muName"]
    dataName=config[poi]["dataName"]

    scan_cmd="scan_poi "+inname+" "+out_name+" "+wsName+" "+muName+" "+dataName+" "+muName+":50:0:5 "+config[poi]["const"]
    os.system(scan_cmd)

print("Scan finished!")
