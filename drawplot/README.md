Scripts to collect the results
- change the 'loc_data' variable in print_mu_1sigma_all.py to point to the location of the scans
- make sure the 'doSyst' property in the last line is set correctly

If the print_mu_1sigma_all.py script is ok, you can run for all mu's in 'parrallel_print_mu.sh'

also, make a directory "LLR_plots"

then run 'plot_NLL_band.py'
