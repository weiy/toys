#ifndef draw1DNLL_cxx
#define draw1DNLL_cxx

#include <stdlib.h>
#include "TChain.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TString.h"
#include "TMultiGraph.h"
#include "TGraphAsymmErrors.h"
#include "TFile.h"
#include "TAxis.h"
#include "TColor.h"
#include "TROOT.h"
#include "TLine.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TTree.h"


#include <string>
#include <iostream>
#include <fstream>
#include "TMath.h"
#include <cmath>        // std::abs

double find_closest_mu(TTree* &physics, double mu, double& status, TString poi_str) {
  const char* nllName = "NLL";
  // const char* variableName = "Gamma";
  const char* statusname = "Status";

  double _nll, _mu, _status;
  physics ->SetBranchAddress(nllName,&_nll);
  // physics ->SetBranchAddress(variableName,&_mu);
  physics ->SetBranchAddress(poi_str,&_mu);
  physics ->SetBranchAddress(statusname,&_status);
  double prev_mu = 0;
  double mu_found = -1;
  double new_NLL = 50;

  int nentries = physics ->GetEntries();
  for(int ientry = 1; ientry < nentries; ientry++){
    physics ->GetEntry(ientry);
    // cout << "mu:" << _mu << " NLL:" << _nll << endl;

    if (abs(mu - prev_mu) >= abs(mu - _mu)) {
      // cout << "set mu to " << _mu << endl;
      new_NLL = _nll;
      status = _status;
      mu_found = _mu;
      // return new_NLL;
    }
    prev_mu = _mu;
  }

  if (new_NLL == 50) {
    cout << "cannot find a mu at all for mu = " << mu << ", something is wrong!" << endl;
    status = 1;
    exit(10);
    return -99;
  }

  // cout << "found closest at mu = " << mu_found << " and NLL = " << new_NLL << endl;
  return new_NLL;
}
  

double find_best_NLL(const char* filename, double& NLLminimum, double& mu_minimum, double& fitNLLminimum, double& fitMUminimum, double& status, TString poi_str) {
  const char* treename = "physics";
  const char* nllName = "NLL";
  // const char* variableName = "Gamma";
  const char* statusname = "Status";

  TFile* fin = TFile::Open(filename,"read");
  TTree* physics = (TTree*) fin->Get(treename);
  if (!physics) {
    cout << "physics not found for file!" << endl;
    cout << filename << endl;
    delete physics;
    fin->Close();
    return -1.;
  }

  double _nll, _mu, _status;
  physics ->SetBranchAddress(nllName,&_nll);
  // physics ->SetBranchAddress(variableName,&_mu);
  physics ->SetBranchAddress(poi_str,&_mu);
  physics ->SetBranchAddress(statusname,&_status);

  int nentries = physics ->GetEntries();
  mu_minimum = 999;
  for(int ientry = 0; ientry < nentries; ientry++){
    physics ->GetEntry(ientry);
    if(ientry == 0) {
      // cout << "first entry: " << _nll << _mu <<endl;
      NLLminimum = _nll;
      fitNLLminimum = _nll;
      mu_minimum = _mu;
      fitMUminimum = _mu;
      status = _status;
      // don't accept mu < 0
      if (mu_minimum < 0) {
        // cout << "mu<0" << endl;
        NLLminimum = 99999.;
      }
    }
    // cout << "mu:" << _mu << " NLL:" << _nll << endl;
    if (_mu < 0) {
      continue;
    }
    if (_nll < NLLminimum) {
      NLLminimum = _nll;
      mu_minimum = _mu;
      status = _status;
      // cout << "NLL minimum now set to " << NLLminimum << endl;
    }
  }

  delete physics;
  fin->Close();

  // std::cout << NLLminimum << " " << mu_minimum << " " << fitNLLminimum << " " << fitMUminimum << " " << status << endl;
  return NLLminimum;
}


double getNLLFromFile(const char* filename, double mymu, double& status, TString poi_str,double yvalue = 1.0)
{
  // setup 
  const char* treename = "physics";
  const char* nllName = "NLL";
  // const char* variableName = "Gamma";
  const char* statusname = "Status";
  double minMass, masslow, masshi;
  double myNLL = 10000.;


  TFile* fin = TFile::Open(filename,"read");
  if(!fin){
    cout<< filename<<" doesn't exist"<<endl;
    exit(1);
  }
  TTree* physics = (TTree*) fin->Get(treename);
  if(!physics){
    cout << "physics not found for file!" << endl;
    cout << filename << endl;
    delete physics;
    fin->Close();
    return -1.;
  }

  vector<double> massvalue;
  vector<double> nllvalue;
  
  vector<double> massvalue_pos;
  vector<double> nllvalue_pos;

  double _nll, _mu;
  double _status;
  physics ->SetBranchAddress(nllName,&_nll);
  // physics ->SetBranchAddress(variableName,&_mu);
  physics ->SetBranchAddress(poi_str,&_mu);
  physics ->SetBranchAddress(statusname,&_status);
  masslow = -999, masshi = -999; 
  int nentries = physics ->GetEntries();

  double lowMassError, hiMassError;
  double lownll, hinll;
  double cvLow = 99999, cvHi = 99999, cvPrev = 99999;
  double minNLL = -999;
  int _int_mu, int_mymu;
  bool found_mu = false;
  // cout<<treename<<" has "<< nentries<<" entries "<<endl;

  for(int ientry = 0; ientry < nentries; ientry++){
    physics ->GetEntry(ientry);
    // cout << "mu:" << _mu << " NLL:" << _nll << endl;
    // compare at 2 decimals
    int_mymu = round(mymu * 100.);
    _int_mu = round(_mu * 100.);

    if (_int_mu == int_mymu) {
      myNLL = _nll;
      status = _status;
      found_mu = true;
      // cout << "found mu = " << mymu << " and NLL is " << myNLL << endl;
    }
    // cout << _status;
    // if (_status != 0) {
    //   cout << "Found non-zero status: " << _status << endl;
    //   cout << "file: " << filename << "\nmu:" << _mu << " NLL:" << _nll << endl;
    // }
  }

  if (!found_mu) {
    // cout << "WARNING: did not find matching mu for mu = " << mymu << endl;
    // cout << "In file " << filename << endl;
    myNLL = find_closest_mu(physics, mymu, status, poi_str);
  }


  // find best fit NLL

  delete physics;
  fin->Close();

  // double best_NLL = find_best_NLL(filename, myNLL);

  // myNLL = myNLL - best_NLL;

  return myNLL;
}

// double test = getNLLFromFile("/project/atlas/users/mveen/Offshell/Workspace/4lMartina/offshellstatistics/H4lOffShellWorkspace/run_toys/submit_STBC_3/mu1/Output_statOnly_mu1/scan_poi_WS_test_statOnly0_mu1.root", 1);

#endif
