from optparse import OptionParser
import os.path
from os import path
import ctypes
import multiprocessing as mp
from glob import glob

import ROOT

def print_mu(file_name, mu, writeTo, poi_str):
    # print(file_name)

    NLLminimum = ctypes.c_double(999)
    mu_minimum = ctypes.c_double(999)
    fitNLLminimum =  ctypes.c_double(999)
    fitMUminimum =  ctypes.c_double(999)
    status =  ctypes.c_double(999)

    status_mu =  ctypes.c_double(999)

    ROOT.find_best_NLL(file_name, NLLminimum, mu_minimum, fitNLLminimum, fitMUminimum, status, poi_str)

    mu = mu.replace("p", ".")
    mu_value = float(mu)
    muNLL = ROOT.getNLLFromFile(file_name, mu_value, status_mu, poi_str)
    if (muNLL == -99):
        return

    short_file = file_name.split("/")[-1]

    # print(status.value)
    if status.value !=0:
        print(file_name+" status is not 0")
    status = float(status.value)
    status_mu = float(status_mu.value)


    myLLR = 2*(muNLL - NLLminimum.value)
    fitLLR = 2*(muNLL - fitNLLminimum.value)

    file = open(writeTo, "a")
    if (mu_minimum.value == fitMUminimum.value):
        file.write("File:{}. For scan with mu = {}, LLR(mu) = {} (status:{:.0f}) and mu-hat = {} (status:{:.0f})\n".format(short_file, mu, myLLR, status_mu, mu_minimum.value, status))
    else:
        file.write("File:{}. For scan with mu = {}, LLR(mu) = {} (status:{:.0f}) and mu-hat = {} (status:{:.0f}). mu-hat changed from {} and LLR(mu) was {}\n".format(short_file, mu, myLLR, status_mu, mu_minimum.value, status, fitMUminimum.value, fitLLR))
    file.close()


# def print_mu_1sigma(mydir, mu, doLumi = "", doSyst = True):
def print_mu_1sigma(mu):

    poi="Rgg"
    poi="Rvv"
    mydir=poi+"_lists/"
    os.system("mkdir -p "+mydir)
    doLumi=""
    doSyst = True
    mu_str = mu
    # loc_data = "/data/mveen/toys_NP_10k_gamma/"
    #loc_data="/data/atlas/atlasdata/weiy/BACKUP/pre_ox17/toys/toys_Gamma_scans_untar/"
    # data_dir_prefix="/data/atlas/atlasdata/weiy/BACKUP/pre_ox17/toys/"
    data_dir_prefix="/data/atlas/atlasdata/weiy/BACKUP/pre_ox17/toys/check_stat_fixed_muVBFOn1p0/"
    data_dir_prefix="/data/atlas/atlasdata/weiy/BACKUP/pre_ox17/toys/check_Rvv_reset/"
    loc_data="toys_Gamma_scans_untar2/"
    loc_data1="toys_Gamma_scans_untar/"
    loc_data2="toys_Gamma_scans_untar2/"

    loc_datadic={"mu":["toys_mu_scans_untar","toys_mu_scans_untar2"],
                 #"Rvv":["toys_Rvv_scans_untar"],
                 "Rgg":["toys_Rgg_scans_untar","toys_Rgg_scans_untar2"],
                 #"Rvv":["toys_Rvv_scans_untar_stat"]
                 #"Rvv":["scan_untar","scan_untar2"],
                 "Rvv":["scan_untar"]}

    i = 0
    outname = "mu_"+str(mu)
    mu_file = mydir+outname+"_1sigma.txt"
    if path.isfile(mu_file):
        print("file mu: ", mu, " already exists!")
        print("remove file")
        os.system("rm -rf "+mu_file)

    scan_mu_files=[]
    print("hello")
    for loc_data in loc_datadic[poi]:
        scan_mu_files+=glob(data_dir_prefix+loc_data+"/*mu"+str(mu)+"*.root")
    print("hello1")

    n_success=0
    for scan_mu_file in scan_mu_files:

        mypath = loc_data+"mu"+mu_str+"_statOnly/scan_poi_WS_test_"+str(i)+"_statOnly.root"
        if doSyst:
            # mypath=loc_data+"/Scan_WS_Gamma_201022_mu"+str(mu)+"_toy"+str(i)+".root"
            mypath=scan_mu_file

        if not path.exists(mypath):
            continue
        print_mu(mypath, mu, mu_file, poi)
        n_success+=1

    print("finished printing mu in file ", mydir+outname+"_1sigma.txt . Total runs: ", n_success)

if __name__ == "__main__":
    usage = "%prog filename:doHL(default:false)"
    version="%prog"
    parser = OptionParser(usage=usage, description="plot mu toys", version=version)
    (options,args) = parser.parse_args()

    if not hasattr(ROOT, "find_best_NLL"):
        ROOT.gROOT.LoadMacro("./plot_NLL_helper.cxx")

    mu_list_syst = ["0p0", "0p1", "0p2", "0p3", "0p4", "0p5", "0p6", "0p7", "0p8", "0p9", 
                    "1p0", "1p1", "1p2", "1p3", "1p4", "1p5", "1p6", "1p7", "1p8", "1p9", 
                    "2p0", "2p1", "2p2", "2p3", "2p4", "2p5", "2p6", "2p7", "2p8", "2p9", 
                    "3p0", "3p1", "3p2", "3p3", "3p4", "3p5", "3p6", "3p7", "3p8", "3p9", 
                    "4p0", "4p1", "4p2", "4p3", "4p4", "4p5", "4p6", "4p7", "4p8", "4p9", 
                    "5p0"]
    # mu_list_syst = ["0p0"]

    if len(args) > 0:
        mu_list_syst = [args[0]]

    pool = mp.Pool(processes=30)
    results = pool.map(print_mu_1sigma, mu_list_syst)
    pool.close()
    # for mu in mu_list_syst:
    #     print_mu_1sigma("mu_lists/", mu, doSyst = True)
