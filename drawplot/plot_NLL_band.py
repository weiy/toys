from optparse import OptionParser
import ROOT
from os import path
import math
import re
import numpy as np
import os

from ROOT import gStyle,TF1

from array import array

# ROOT.gStyle.SetOptTitle(0)
# ROOT.gStyle.SetOptStat(0)
# ROOT.gStyle.SetOptFit(0)
ROOT.gROOT.SetBatch()


xbins_data = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0]

xbins_data_syst = xbins_data

# xbins_data = [x/10. for x in range(0, 51)]

x_prev = 0
xbins = []
for x in xbins_data:
    shift = 0.5 * (x - x_prev)
    # shift half the difference with the previous
    xbin = x - shift
    xbins.append(xbin)
    x_prev = x
# add one more bin, based on the last shift
xbins.append(xbins_data[-1] + shift)

ymax = 8
ymin = 0
ystepsize_inverse = 100
ybins = (ymax-ymin)*ystepsize_inverse

# poi="Rvv"
# poi="Rgg"
poi="mu"

titles={"Gamma":"#Gamma/#Gamma^{SM}",
        "mu":"#mu_{offshell}",
        "Rgg":"R_{gg}",
        "Rvv":"R_{VV}"}

# SCRIPT_DIR = "/project/atlas/users/mveen/Offshell/Workspace/4lMartina/offshellstatistics/H4lOffShellWorkspace/run_toys/submit_final/Results/"
SCRIPT_DIR = path.dirname(__file__)
SCRIPT_DIR = poi+"_lists/"

os.system("mkdir -p "+SCRIPT_DIR)
os.system("mkdir -p "+poi+"_plots/LLR_plots_syst")

def getListFromFile(f_path, doFitResult):
    LLR_list = []
    muhat_list = []
    LLR_list_fit = []
    muhat_list_fit = []

    myfile = open(f_path, 'r')
    alllines = myfile.readlines()

    print("Opened file ", f_path, " with ", len(alllines), " lines")

    for line in alllines:
            # txt = re.search("mu-hat LLR: ([0\.-9,-,e-]+)", line)
            txt = re.search("For scan with mu = ([0-9.]+), LLR\(mu\) = ([0-9.,e,-]+) \(status:([0-9])\) and mu-hat = ([-,0-9.]+) \(status:([0-9])\)", line)
            txt2 = re.search("mu-hat changed from ([0-9.,e,-]+) and LLR\(mu\) was ([0-9.,e,-]+)", line)
            if txt:
                status = int(txt.group(3)) + int(txt.group(5))
                if not status == 0:
                    continue
                LLR = float(txt.group(2))
                muhat = float(txt.group(4))
                LLR_before = LLR
                muhat_before = muhat

                if txt2:
                    muhat_before = float(txt2.group(1))
                    if muhat_before > 0:
                        LLR_before = float(txt2.group(2))
                    else:
                        muhat_before = muhat

                if doFitResult:
                    LLR = LLR_before
                    muhat = muhat_before

                LLR_list.append(LLR)
                muhat_list.append(muhat)
                LLR_list_fit.append(LLR_before)
                muhat_list_fit.append(muhat_before)
                # print(muhat_before)

    print("processes ", len(LLR_list), " toys in ", f_path)
    # print(LLR_list)
    return LLR_list, muhat_list, LLR_list_fit, muhat_list_fit

def makeLLRFile(path, LLR_list):
    # print(LLR_list[0][1].split("/")[-1].split(".root")[0])
    # exit()

    print("making file: ", path)
    LLR_file = open(path, "w")

    for LLR, filepath in LLR_list:
        filename = filepath.split("/")[-1].split(".root")[0]
        LLR_file.write("mu-hat LLR: " + str(LLR) + " in file " + filename + "\n")

    LLR_file.close()

def invert(hist, ybins = ybins):
    hist_band_invt = ROOT.TH2D("hist_band_invt", "hist_band_invt", ybins, ymin, ymax, len(xbins)-1, array('d',xbins))
    nbinsX = hist.GetNbinsX()
    nbinsY = hist.GetNbinsY()
    for myXbin in range(1,nbinsX+1):
        for myYbin in range(1,nbinsY+1):
            hist_band_invt.SetBinContent(myYbin, myXbin, hist.GetBinContent(hist.GetBin(myXbin, myYbin, 0)))

    hist_band_invt.SetTitle("negative-log-likelihood of toy MC")
    hist_band_invt.GetYaxis().SetTitle(titles[poi])
    hist_band_invt.GetXaxis().SetTitle("Likelihood Ratio")

    return hist_band_invt

def interpolate_mu(x_CL, low_CL, high_CL, low_mu, high_mu):
    new_mu = low_mu + ((x_CL - low_CL)/(high_CL - low_CL) * (high_mu - low_mu))
    print("x_CL, low_CL, high_CL, low_mu, high_mu, new_mu: ", x_CL, ", ", low_CL, ", ", high_CL, ", ", low_mu, ", ", high_mu, ", ", new_mu)
    return new_mu

# take a certain interval of mu values
def take_mu_interval(sigma, doLimit, mu_list, do_interpolate = False):
    if sigma == 1:
        # CL = 0.6827
        CL = 0.682689492
    elif sigma == 2:
        # CL = 0.9545
        CL = 0.954499736
    else:
        print("no value hardcoded for sigma = ", sigma)
        exit()

    P = 1.-CL
    mu_list.sort()

    # events excluded
    # upper limit
    if doLimit > 0:
        n_border = int(round(P * len(mu_list) ))
        mu_list_confidence = mu_list[n_border:]
        # if setting an upper limit, we assume the first value is 0:
        mu_low = mu_list_confidence[0]
        mu_high = ymax
    # lower limit (used for NLL plots)
    elif doLimit < 0:
        n_border = int(round(P * len(mu_list) ))
        mu_list_confidence = mu_list[:-n_border]
        # if setting an upper limit, we assume the first value is 0:
        mu_low = 0
        mu_high = mu_list_confidence[-1]
        if do_interpolate:
            mu_list_confidence_2 = mu_list[:-(n_border-1)]
            x_CL = CL
            low_CL = 1-n_border/float(len(mu_list))
            high_CL = 1-(n_border-1)/float(len(mu_list))
            low_mu = mu_high
            high_mu = mu_list_confidence_2[-1]
            interpolate_mu(x_CL, low_CL, high_CL, low_mu, high_mu)
    else:
        n_border = int(round(P * len(mu_list) / 2. ))
        mu_list_confidence = mu_list[n_border:-n_border]
        mu_low =mu_list_confidence[0]
        mu_high = mu_list_confidence[-1]


    return mu_low, mu_high

def plot_mu_band(doHL = False, doAltPull = True, doinvert = True, sigma = 1, doLimit = False, doSyst = False, do_interpolate = False, xbins_toprocess = xbins_data, doFitResult = False, doDensity = False):
    # xbins_data_syst = [0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3, 3.2, 3.4, 3.6, 3.8, 4, 4.2, 4.4, 4.6, 4.8, 5]
    # xbins_data_syst = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5]

    print(xbins_toprocess)

    gStyle.SetOptStat(0);
    # gStyle.SetOptTitle(0);
    gStyle.SetLineWidth(2);
    size = 0.05;
    gStyle.SetLabelSize(size);
    gStyle.SetTitleSize(size);
    gStyle.SetLabelSize(size, "Y");
    gStyle.SetTitleSize(size, "Y");
    # set ticks on left Yaxis
    gStyle.SetPadTickY(1)

    thecanvas = ROOT.TCanvas( "thecanvas", "thecanvas", 1000, 800 )
    thecanvas.SetBottomMargin(0.125)
    thecanvas.SetLeftMargin(0.125)
    thecanvas.SetGrid(1)

    if doDensity:
        ystepsize_inverse = 10
        ybins = (ymax-ymin)*ystepsize_inverse
    else:
        ystepsize_inverse = 100
        ybins = (ymax-ymin)*ystepsize_inverse


    hist_band_mu = ROOT.TH2D("hist_density", "hist_band_mu", len(xbins)-1, array('d',xbins), ybins, ymin, ymax)
    hist_band_mu2 = ROOT.TH2D("hist_density2", "hist_band_mu2", len(xbins)-1, array('d',xbins), ybins, ymin, ymax)
    if doSyst:
        hist_band_mu2.SetTitle("measured "+titles[poi]+" vs true "+titles[poi]+" with systematics")
    else:
        hist_band_mu2.SetTitle("measured "+titles[poi]+" vs true "+titles[poi])

    hist_LLR_empty = ROOT.TH1F("hist_empty", "hist_empty", 50, 0, 5)
    hist_LLR = ROOT.TH1F("hist_LLR", "hist_LLR", 50, 0, 5)
    hist_band = ROOT.TH2D("hist_LLR_density", "hist_band", len(xbins)-1, array('d',xbins), ybins, ymin, ymax)
    hist_band2 = ROOT.TH2D("hist_LLR_density2", "hist_band2", len(xbins)-1, array('d',xbins), ybins, ymin, ymax)
    hist_band.SetTitle("negative-log-likelihood ratio of toy MC")
    hist_band2.SetTitle("negative-log-likelihood ratio of toy MC")
    hist_band.GetXaxis().SetTitle(titles[poi])
    hist_band.GetYaxis().SetTitle("Likelihood Ratio")
    hist_band2.GetXaxis().SetTitle(titles[poi])
    hist_band2.GetYaxis().SetTitle("Likelihood Ratio")

    hist_band_simple_1 =  ROOT.TH1F("hist_LLR_1", "hist_LLR_1", len(xbins)-1, array('d',xbins))
    hist_band_simple_2 =  ROOT.TH1F("hist_LLR_2", "hist_LLR_2", len(xbins)-1, array('d',xbins))

    hist_band_simple_1.GetXaxis().SetTitle(titles[poi])
    hist_band_simple_1.GetYaxis().SetTitle("Likelihood Ratio")
    hist_band_simple_2.GetXaxis().SetTitle(titles[poi])
    hist_band_simple_2.GetYaxis().SetTitle("Likelihood Ratio")
    # hist_band.Sumw2()

    hist_delta_mu = ROOT.TH1F("hist_delta_mu", "hist_delta_mu", 50, 0, 5)
    hist_delta_mu.SetTitle("Difference in "+titles[poi]+" between fit and scan")
    hist_delta_mu.GetXaxis().SetTitle("#Delta"+titles[poi])
    hist_delta_mu.GetYaxis().SetTitle("nr. of toys")

    hist_delta_LLR = ROOT.TH1F("hist_delta_LLR", "hist_delta_LLR", 60, -1, 5)
    hist_delta_LLR.SetTitle("Difference in LLR between fit and scan")
    hist_delta_LLR.GetXaxis().SetTitle("#DeltaLLR")
    hist_delta_LLR.GetYaxis().SetTitle("nr. of toys")

    suffix = ""
    if doHL:
        # suffix = "_HL_new"
        suffix = "_HL"
    if doSyst:
        suffix = suffix + "_syst"
    if doFitResult:
        suffix = "_FitResult"

    list_upper_limits_1s = []
    list_upper_limits_2s = []

    # new method: use x bins for each mu:
    for mu in xbins_toprocess:
        mu_str = str(mu).replace(".", "p")
        hist_LLR.Reset("ICESM")
        hist_delta_mu.Reset("ICESM")
        hist_delta_LLR.Reset("ICESM")
        #Reset: Integral, Content, Errors, Statistics, Mini and Maximum
        #https://root.cern/doc/master/classTH1.html#a537509270db4e59efb39619b94c3a9a4

        LLR_file = "mu_"+mu_str+"_1sigma.txt"
        LLR_file_out = open(poi+"_plots/LLR_"+mu_str+".txt","w")
        LLR_file_path = SCRIPT_DIR+LLR_file
        if (path.exists(LLR_file_path)):
            LLR_list, muhat_list, LLR_list_fit, muhat_list_fit = getListFromFile(LLR_file_path, doFitResult)
            
            for myLLR in LLR_list:
                hist_LLR.Fill(myLLR)
                LLR_file_out.write(str(myLLR)+"\n")

            for muhat, muhat_fit in zip(muhat_list,muhat_list_fit):
                hist_delta_mu.Fill(abs(muhat - muhat_fit))
                # print(abs(muhat - muhat_fit))
            for myLLR, LLR_fit in zip(LLR_list,LLR_list_fit):
                hist_delta_LLR.Fill(myLLR - LLR_fit)

        else:
            print("File not found: ", LLR_file_path)
            return
            # makeLLRFile(LLR_file_path, LLR_and_path_list)

        thecanvas.SetLogy()
        hist_LLR.SetTitle("negative-log-likelihood ratio of toy MC with "+titles[poi]+"="+ str(mu))
        if doHL: hist_LLR.SetTitle("negative-log-likelihood ratio of high lumi toy MC with "+titles[poi]+"="+ str(mu))
        hist_LLR.GetYaxis().SetTitle("nr. of Toys")
        hist_LLR.GetXaxis().SetTitle("Likelihood Ratio")
        hist_LLR.SetMinimum(1)
        hist_LLR.SetMaximum(1e4)

        LLR_down, LLR_up = take_mu_interval(1, -1, LLR_list, do_interpolate)
        list_upper_limits_1s.append(LLR_up)
        LLR_down2, LLR_up2 = take_mu_interval(2, -1, LLR_list, do_interpolate)
        list_upper_limits_2s.append(LLR_up2)
        hist_band_simple_1.SetBinContent(hist_band_simple_1.FindBin(mu), LLR_up)
        hist_band_simple_2.SetBinContent(hist_band_simple_2.FindBin(mu), LLR_up2)
        for ystep in range(ymin * ystepsize_inverse, ymax * ystepsize_inverse + ystepsize_inverse):
            ybin = float(ystep) / float(ystepsize_inverse)
            yvalue = ybin - 0.5 / float(ystepsize_inverse)
            if yvalue < 0: yvalue = 0
            if (ybin >= LLR_down and ybin <= LLR_up):
                hist_band.Fill(mu, yvalue, 10)
            if (ybin >= LLR_down2 and ybin <= LLR_up2):
                hist_band2.Fill(mu, yvalue, 1)

        if doDensity:
            for muhat in muhat_list:
                hist_band_mu.Fill(mu, muhat)
                # hist_band2.Fill(mu, muhat)
        else:
            mu_down, mu_up = take_mu_interval(1, doLimit, muhat_list)
            mu_down2, mu_up2 = take_mu_interval(2, doLimit, muhat_list)
            for ystep in range(ymin * ystepsize_inverse, ymax * ystepsize_inverse + ystepsize_inverse):
                ybin = float(ystep) / float(ystepsize_inverse)
                yvalue = ybin - 0.5 / float(ystepsize_inverse)
                if (ybin >= mu_down and ybin <= mu_up):
                    hist_band_mu.Fill(mu, yvalue, 10)
                if (ybin >= mu_down2 and ybin <= mu_up2):
                    hist_band_mu2.Fill(mu, yvalue, 1)



        # hist_LLR.Scale(1./float(len(LLR_list)))
        hist_LLR.Draw("PE")
        # line at 68%
        line = ROOT.TLine(LLR_up, 0, LLR_up, hist_LLR.GetMaximum())
        line.Draw("SAME")

        # fit chi2 with 1 dof
        # chi2 = ROOT.TF1("chi2","TMath::Prob(x,[0])*1000",0,ymax)
        # chi2.SetParameter(0,1)
        # chi2.Draw("SAME")
        # hist_LLR.Fit("chi2")
        # hist_LLR.Fit("TMath::Prob(x,1)")

        thecanvas.SaveAs(poi+"_plots/LLR_plots"+suffix+"/"+poi+"_LLR_mu"+mu_str+".png")

        hist_delta_mu.Draw("HIST")
        thecanvas.SaveAs(poi+"_plots/LLR_plots"+suffix+"/"+poi+"_delta_mu"+mu_str+".png")

        hist_delta_LLR.Draw("HIST")
        thecanvas.SaveAs(poi+"_plots/LLR_plots"+suffix+"/delta_LLR"+mu_str+".png")

    # if only one bin to fill the file, stop here
    if len(xbins_toprocess) < len (xbins_data_syst):
        return

    thecanvas.SetLogy(0)

    # hist_band_simple_1.SetFillColor()
    hist_band_simple_2.SetMaximum(ymax)
    hist_band_simple_2.SetMinimum(ymin)
    hist_band_simple_2.SetFillColor( ROOT.kBlue)
    hist_band_simple_2.SetFillStyle(1001)
    hist_band_simple_1.SetFillColor( ROOT.kYellow )
    hist_band_simple_1.SetFillStyle(1001)

    # fit a poly through the hist
    # hist_band_simple_2.Fit("pol6")
    # fpoly=TF1("fpoly", "[0]+[1]*x+[2]*x**2+[3]*x**3+[4]*x**4+[5]*x**5+[6]*x**6+[7]*x**7+[8]*x**8+[9]*x**9+[10]*x**10+[11]*x**11+[12]*sin(x)", 0., 5.)
    fpoly=TF1("fpoly", "[0]+[1]*x+[2]*x**2+[3]*x**3+[4]*x**4+[5]*x**5+[6]*x**6+[7]*x**7+[8]*x**8", 0., 5.)
    hist_band_simple_2.Fit("fpoly", "", "", xbins_data[1], xbins_data[-1])
    hist_band_simple_1.Fit("fpoly", "", "", xbins_data[1], xbins_data[-1])


    hist_band_simple_2.Draw("")
    hist_band_simple_1.Draw("SAME")

    graph_file = ROOT.TFile(poi+"_GraphsFit"+suffix+".root", "recreate")
    graph_file.cd()

    func_1s = hist_band_simple_1.GetFunction("fpoly");
    func_2s = hist_band_simple_2.GetFunction("fpoly");

    # extrapolate 1s
    func_1s.SetRange(0,5)
    func_2s.SetRange(0,5)

    func_1s.SetTitle("1s_line")
    func_1s.SetName("1s_line")
    func_1s.Write()
    func_2s.SetTitle("2s_line")
    func_2s.SetName("2s_line")
    func_2s.Write()

    graph_file.Close()

    thecanvas.SaveAs(poi+"_plots/"+poi+"_simpleHist"+suffix+".png")

    if doLimit:
        suffix = suffix + "_Limit"
    if do_interpolate:
        suffix = suffix + "_interpolate"

    if doDensity:
        themax = 600.
        hist_band_mu.SetMinimum(0.)
        hist_band_mu.SetMaximum(themax)
        hist_band_mu.Draw("COL")
        thecanvas.RedrawAxis("g")
        thecanvas.SaveAs(poi+"_plots/"+poi+"_Confidence_density_inverted.png")

        hist_band_invt_mu = invert(hist_band_mu, ybins)
        hist_band_invt_mu.SetMinimum(0.)
        hist_band_invt_mu.SetMaximum(themax)
        hist_band_invt_mu.Draw("COL")
        hist_band_invt_mu.GetYaxis().SetTitle(titles[poi])
        hist_band_invt_mu.GetXaxis().SetTitle("#hat{"+titles[poi]+"}")
        thecanvas.SaveAs(poi+"_plots/"+poi+"_Confidence_density.png")
        return


    if doinvert:
        hist_band_invt = invert(hist_band)
        hist_band_invt.SetMinimum(0.)
        hist_band_invt2 = invert(hist_band2)
        hist_band_invt2.SetMinimum(0.)
        hist_band_invt2.SetMaximum(10.)
        hist_band_invt.SetMaximum(100.)
        hist_band_invt2.Draw("COL")
        hist_band_invt.Draw("COL SAME")
        thecanvas.RedrawAxis("g")
        thecanvas.SaveAs(poi+"_plots/"+poi+"_ConfidenceLLR"+suffix+".png")

        hist_band_invt_mu = invert(hist_band_mu)
        hist_band_invt_mu.SetMinimum(0.)
        hist_band_invt_mu2 = invert(hist_band_mu2)
        hist_band_invt_mu2.SetMinimum(0.)
        hist_band_invt_mu2.SetMaximum(10.)
        if doSyst:
            hist_band_invt_mu2.SetTitle("measured "+titles[poi]+" vs true "+titles[poi]+" with systematics")
        else:
            hist_band_invt_mu2.SetTitle("measured "+titles[poi]+" vs true "+titles[poi])
        hist_band_invt_mu2.GetYaxis().SetTitle(titles[poi])
        hist_band_invt_mu2.GetXaxis().SetTitle("#hat{"+titles[poi]+"}")
        hist_band_invt_mu2.Draw("COL")
        hist_band_invt_mu.Draw("COL SAME")
        thecanvas.RedrawAxis("g")
        thecanvas.SaveAs(poi+"_plots/"+poi+"_Confidence"+suffix+".png")


    hist_band.SetMinimum(0.)
    hist_band2.SetMinimum(0.)
    hist_band2.SetMaximum(10.)
    # hist_band.SetMaximum(0.6)
    hist_band2.Draw("COL")
    hist_band.Draw("COL SAME")
    thecanvas.RedrawAxis("g")

    thecanvas.SaveAs(poi+"_plots/"+poi+"_ConfidenceLLR"+suffix+"_inverted.png")

    hist_band_mu.SetMinimum(0.)
    hist_band_mu2.SetMinimum(0.)
    hist_band_mu2.SetMaximum(10.)
    hist_band_mu2.Draw("COL")
    hist_band_mu.Draw("COL SAME")
    thecanvas.RedrawAxis("g")
    thecanvas.SaveAs(poi+"_plots/"+poi+"_Confidence"+suffix+"_inverted.png")

    # depreciated graph method
    return

    graph_file = ROOT.TFile(poi+"_Graphs"+suffix+".root", "recreate")

    # take out point mu0 from 1sigma list
    # list_upper_limits_1s= list_upper_limits_1s[1:]
    ignore_firstbin = True
    if ignore_firstbin:
        graph_1s = ROOT.TGraph(len(xbins_data)-1, np.array(xbins_data[1:], dtype = 'float'), np.array(list_upper_limits_1s[1:], dtype = 'float'))
    else:
        graph_1s = ROOT.TGraph(len(xbins_data), np.array(xbins_data, dtype = 'float'), np.array(list_upper_limits_1s, dtype = 'float'))
    graph_1s.SetLineWidth(2)
    graph_1s.Draw()
    graph_1s.SetName("1s_line")
    graph_1s.Write()

    smoother = ROOT.TGraphSmooth()
    # https://root.cern.ch/doc/master/classTGraphSmooth.html
    graph_smooth = smoother.SmoothKern(graph_1s)
    graph_smooth.SetLineWidth(2)
    graph_smooth.SetLineColor(2)
    graph_smooth.SetName("1s_line_smooth")
    graph_smooth.Draw()

    graph_smooth.Write()

    graph_2s = ROOT.TGraph(len(xbins_data), np.array(xbins_data, dtype = 'float'), np.array(list_upper_limits_2s, dtype = 'float'))
    graph_2s.SetLineWidth(2)
    graph_2s.Draw()
    graph_2s.SetName("2s_line")
    graph_2s.Write()

    smoother2 = ROOT.TGraphSmooth()
    # https://root.cern.ch/doc/master/classTGraphSmooth.html
    graph_2s_smooth = smoother2.SmoothKern(graph_2s)
    graph_2s_smooth.SetLineWidth(2)
    graph_2s_smooth.SetLineColor(2)
    graph_2s_smooth.SetName("2s_line_smooth")
    graph_2s_smooth.Draw()

    graph_2s_smooth.Write()


    thecanvas.SaveAs("./ConfidenceLLR"+suffix+"_graph.png")

    del graph_file

    return


if __name__ == "__main__":
    usage = "%prog filename:doHL(default:false)"
    version="%prog"
    parser = OptionParser(usage=usage, description="plot mu toys", version=version)
    (options,args) = parser.parse_args()

    doHL = False
    # doSyst = False
    doDensity = False
    doSyst = True #weiy
    myxbins = xbins_data
    if len(args) > 0:
        if "doSys" in args[0]:
            doSyst = True
            # myxbins = xbins_data_syst
        elif args[0] == "doHL":
            doHL = True
        elif args[0] == "density":
            doDensity = True

        if "bin" in args[0]:
            mybin = args[0].split("bin")[1]
            mybins_str = mybin.split(",")
            myxbins = []
            for i_bin in mybins_str:
                try:
                    i_bin = int(i_bin)
                    myxbins.append(i_bin)
                except:
                    i_bin = float(i_bin)
                    myxbins.append(i_bin)

    #            False       True            False                 [0.0,0.1,...,5.0]            False              False
    plot_mu_band(doHL, doSyst = doSyst, do_interpolate = False, xbins_toprocess = myxbins, doFitResult = False, doDensity = doDensity)
    os.system("bash copy_to_eos.sh")
    exit()
